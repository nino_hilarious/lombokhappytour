<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sluggable;

class Destination extends Model
{
    use Sluggable;

    protected $fillable = [
        'title','content','image','location_id','title_en','content_en','meta_title','meta_description'
        ,'meta_title_en','meta_description_en','alt','alt_en'
    ];

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;

        $this->attributes['slug'] = $this->slug($title, '');
    }

    public function imageDestination()
    {
        return $this->hasMany('\App\ImageDestination');
    }

    public function location()
    {
        return $this->belongsTo('\App\LocationDestination');
    }
}
