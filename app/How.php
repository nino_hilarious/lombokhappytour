<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class How extends Model
{
    protected $fillable = [
        'content','content_en','meta_title','meta_description','meta_title_en','meta_description_en'
    ];
}
