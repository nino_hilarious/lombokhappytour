<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sluggable;

class LocationDestination extends Model
{
    use Sluggable;

    protected $fillable = [
        'location','location_en'
    ];

    public function setLocationAttribute($title)
    {
        $this->attributes['location'] = $title;

        $this->attributes['slug'] = $this->slug($title, '');
    }

    public function destination()
    {
        return $this->hasMany('App\Destination', 'location_id');
    }
}
