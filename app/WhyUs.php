<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhyUs extends Model
{
    protected $table = 'why_uses';

    protected $fillable = [
        'content','content_en','meta_title','meta_description','meta_title_en','meta_description_en'
    ];
}
