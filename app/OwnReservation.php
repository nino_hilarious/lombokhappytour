<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnReservation extends Model
{
    protected $table='own_reservations';

    protected $fillable = [
        'name','email','address','phone','destination','people','day','hotel','date','message'
    ];
}
