<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Http\Requests;

trait Images {

    public function Image(Request $request)
    {
        $input = $request->file('image');
        $extension = $input->getClientOriginalExtension();
        $filename = md5(time()) . '.' . $extension;
        $path = public_path() . DIRECTORY_SEPARATOR . 'img';

        $upload = $input->move($path, $filename);
        return array($filename, $upload);
    }

    public function DeleteImage($image)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $image->image;

        \File::delete($path);
    }

}