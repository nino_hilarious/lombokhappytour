<?php

namespace App\Traits;

trait Sluggable {
    public function slug($title, $unique)
    {
        $slug = str_slug($title. '-'. $unique);

        if(static::whereSlug($slug)->exists())
        {
            return $this->slug($title, $unique + 1);
        }

        return $this->attributes['slug'] = $slug;
    }
}