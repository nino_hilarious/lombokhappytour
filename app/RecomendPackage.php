<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sluggable;

class RecomendPackage extends Model
{
    use Sluggable;

    protected $fillable = [
        'content','title','image','title_en','content_en','meta_title','meta_description','meta_title_en','meta_description_en'
    ];

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;

        $this->attributes['slug'] = $this->slug($title, '');
    }
}
