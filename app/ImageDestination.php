<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageDestination extends Model
{
    protected $fillable = [
        'image','destination_id'
    ];

    public function destination()
    {
        return $this->belongsTo('\App\Destination');
    }
}
