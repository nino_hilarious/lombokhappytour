<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecomendReservation extends Model
{
    protected $table = 'recomend_reservations';

    protected $fillable = [
        'name','email','address','phone','people','message','package','date'
    ];
}
