<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sluggable;

class News extends Model
{
    use Sluggable;

    protected $fillable = [
        'title','title_en','content','content_en','meta_title','meta_description','meta_title_en','meta_description_en'
    ];

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;

        $this->attributes['slug'] = $this->slug($title, '');
    }
}
