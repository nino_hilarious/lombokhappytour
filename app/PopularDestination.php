<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularDestination extends Model
{
    protected $fillable = [
        'content', 'title', 'image'
    ];
}
