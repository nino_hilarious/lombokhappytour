<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OwnReservationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:recomend_reservations,email',
            'address' => 'required|min:5',
            'phone' => 'required|numeric|unique:recomend_reservations,phone',
            'people' => 'required|numeric',
            'destination' => 'required',
            'day' => 'required|numeric',
            'date' => 'required|date'
        ];
    }
}
