<?php

/*************************** Frontend *********************************/
Route::get('/', function () {
    return view('frontend.front');
});

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]], function()
{
    Route::get('pages', 'FrontendController@index');
    Route::get('pages/destination', 'FrontendController@destination');
    Route::get('pages/destination/{slug}', 'FrontendController@showDestination');
    Route::get('pages/location/{slug}', 'FrontendController@locationDestination');
    Route::get('pages/why-us', 'FrontendController@whyus');
    Route::get('pages/recomend-package', 'FrontendController@recomendPackage');
    Route::get('pages/recomend-package/{slug}', 'FrontendController@showRecomendPackage');
    Route::get('pages/how-to-reservation', 'FrontendController@howToReservation');
    Route::get('pages/recomend-reservation', 'RecomendReservationController@index');
    Route::post('pages/recomend-reservation', 'RecomendReservationController@store');
    Route::get('pages/own-reservation', 'OwnReservationController@index');
    Route::post('pages/own-reservation', 'OwnReservationController@store');
    Route::get('pages/news', 'FrontendController@news');
    Route::get('pages/news/{slug}', 'FrontendController@showNews');
    /*************************** End Frontend *********************************/
});

/*************************** Login *********************************/
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
/*************************** End Login *********************************/

/*************************** Backend *********************************/
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    //Routing for dashboard
    Route::get('/', 'DashboardController@index');
    //Routing for Location Destination
    Route::resource('location', 'LocationDestinationController');
    //Routing for Destination
    Route::resource('destination', 'DestinationController');
    //Routing for Image Destination
    Route::resource('image', 'ImageDestinationController');
    //Routing for What is Lombok
    Route::get('lombok', 'LombokController@index');
    Route::get('lombok/{id}/edit', 'LombokController@edit');
    Route::patch('lombok/{id}', 'LombokController@update');
    //Routing for Recommended Package
    Route::resource('recomend-package', 'RecomendPackageController');
    //Routing for Popular Destination
    Route::resource('popular-destination', 'PopularDestinationController');
    //Routing for Why Us
    Route::get('whyus', 'WhyUsController@index');
    Route::get('whyus/{id}/edit', 'WhyUsController@edit');
    Route::patch('whyus/{id}', 'WhyUsController@update');
    //Routing for How to Reservation
    Route::get('how', 'HowController@index');
    Route::get('how/{id}/edit', 'HowController@edit');
    Route::patch('how/{id}', 'HowController@update');
    //Routing for Reservation
    /* For Own Reservation */
    Route::get('own-reservation', 'OwnReservationController@show');
    Route::delete('own-reservation/{id}', 'OwnReservationController@destroy');
    /* For Recomend Reservation */
    Route::get('recomend-reservation', 'RecomendReservationController@show');
    Route::delete('recomend-reservation/{id}', 'RecomendReservationController@destroy');
    /* Routing For News */
    Route::resource('news','NewsController');

    /*************** FOR ENGLISH LANGUAGE *****************/
    Route::get('en/lombok', 'LombokEnController@index');
    Route::get('en/lombok/{id}', 'LombokEnController@edit');
    Route::patch('en/lombok/{id}', 'LombokEnController@update');

});
/*************************** End Backend *********************************/