<?php

namespace App\Http\Controllers;

use App\RecomendPackage;
use App\RecomendReservation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class RecomendReservationController extends Controller
{
    public function index()
    {
        if(LaravelLocalization::getCurrentLocale() === 'id')
            $recomend = RecomendPackage::lists('title', 'title');
        else
            $recomend = RecomendPackage::lists('title_en', 'title_en');

        return view('frontend.pages.recomend_reservation', compact('recomend'));
    }

    public function store(Requests\RecomendReservationRequest $request)
    {
        RecomendReservation::create($request->all());

        $request->session()->flash('status', 'Terimakasih! Pemesanan anda sudah terkirim. Kami akan segera menghubungi anda. :)');

        return redirect('pages/recomend-reservation');
    }

    public function show()
    {
        $all = RecomendReservation::paginate(10);

        return view('backend.reservation.recomend_reservation', compact('all'));
    }

    public function destroy($id)
    {
        $reservation = RecomendReservation::findOrFail($id);

        $reservation->delete();

        return redirect('admin/recomend-reservation');
    }
}
