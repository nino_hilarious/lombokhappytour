<?php

namespace App\Http\Controllers;

use App\How;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HowController extends Controller
{
    public function index()
    {
        $how = How::all();

        return view('backend.how.index', compact('how'));
    }

    public function edit($id)
    {
        $how = How::findOrFail($id);

        return view('backend.how.edit', compact('how'));
    }

    public function update($id, Request $request)
    {
        $how = How::findOrFail($id);

        $how->update($request->all());

        return redirect('admin/how');
    }
}
