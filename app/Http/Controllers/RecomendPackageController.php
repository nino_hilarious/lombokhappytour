<?php

namespace App\Http\Controllers;

use App\RecomendPackage;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Traits\Images;

class RecomendPackageController extends Controller
{
    use Images;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $packages = RecomendPackage::all();

        return view('backend.package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $image = RecomendPackage::create($request->except('image'));

        if($request->hasFile('image'))
        {
            list($filename, $upload) = $this->Image($request);

            Image::make($upload)->resize(300, 300)->save();

            $image->image = $filename;
            $image->save();
        }

        return redirect('admin/recomend-package');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $package = RecomendPackage::findOrFail($id);

        return view('backend.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $image = RecomendPackage::findOrFail($id);

        if ($request->hasFile('image'))
        {
            list($filename, $upload) = $this->Image($request);

            if($image->image)
            {
                $this->DeleteImage($image);
            }

            Image::make($upload)->resize(300, 300)->save();

            $image->image = $filename;
            $image->save();
        }

        if (!$image->update($request->except('image')))
        {
            return Redirect::back();
        }

        return redirect('admin/recomend-package');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = RecomendPackage::findOrFail($id);

        if ($image->image)
        {
            $this->DeleteImage($image);
        }

        $image->delete();


        return redirect('admin/recomend-package');
    }
}
