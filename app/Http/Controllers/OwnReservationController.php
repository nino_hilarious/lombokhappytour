<?php

namespace App\Http\Controllers;

use App\Destination;
use App\OwnReservation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class OwnReservationController extends Controller
{
    public function index()
    {
        if(LaravelLocalization::getCurrentLocale() === 'id')
            $destination = Destination::lists('title','title');
        else
            $destination = Destination::lists('title_en','title_en');

        return view('frontend.pages.own_reservation', compact('destination'));
    }

    public function store(Requests\OwnReservationRequest $request)
    {
        $data = $request->all();
        $data['destination'] = implode(',', $request->input('destination'));
        OwnReservation::create($data);

        $request->session()->flash('status', 'Terimakasih! Pemesanan anda sudah terkirim. Kami akan segera menghubungi anda. :)');

        return redirect('pages/own-reservation');
    }

    public function show()
    {
        $all = OwnReservation::paginate(10);

        return view('backend.reservation.own_reservation', compact('all'));
    }

    public function destroy($id)
    {
        $reservation = OwnReservation::findOrFail($id);

        $reservation->delete();

        return redirect('admin/own-reservation');
    }
}
