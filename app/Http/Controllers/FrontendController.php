<?php

namespace App\Http\Controllers;

use App\Destination;
use App\How;
use App\LocationDestination;
use App\Lombok;
use App\LombokEn;
use App\News;
use App\PopularDestination;
use App\RecomendPackage;
use App\WhyUs;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    public function index()
    {
        $lombok = Lombok::findorFail(1);

        return view('frontend.pages.index', compact('lombok'));
    }

    public function destination()
    {
        $destinations = Destination::paginate(6);
        $locations = LocationDestination::all();

        return view('frontend.pages.destination', compact('destinations', 'locations'));
    }

    public function showDestination($slug)
    {
        $destination = Destination::whereSlug($slug)->firstOrFail();

        return view('frontend.pages.show_destination', compact('destination'));
    }

    public function whyus()
    {
        $whyus = WhyUs::findOrFail(1);

        return view('frontend.pages.whyus', compact('whyus'));
    }

    public function recomendPackage()
    {
        $recomends = RecomendPackage::all();

        return view('frontend.pages.recomend', compact('recomends'));
    }

    public function showRecomendPackage($slug)
    {
        $recomend = RecomendPackage::whereSlug($slug)->firstOrFail();

        return view('frontend.pages.show_recomend', compact('recomend'));
    }

    public function howToReservation()
    {
        $how = How::findOrFail(1);

        return view('frontend.pages.how_reservation', compact('how'));
    }

    public function locationDestination($slug)
    {
        $location = LocationDestination::whereSlug($slug)->firstOrFail();
        $locations = LocationDestination::all();

        return view('frontend.pages.location', compact('location','locations'));
    }

    public function news()
    {
        $news = News::latest('created_at')->paginate(5);

        return view('frontend.pages.news', compact('news'));
    }

    public function showNews($slug)
    {
        $news = News::whereSlug($slug)->firstOrFail();

        return view('frontend.pages.show_news', compact('news'));
    }
}
