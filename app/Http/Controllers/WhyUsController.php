<?php

namespace App\Http\Controllers;

use App\WhyUs;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WhyUsController extends Controller
{
    public function index()
    {
        $whyus = WhyUs::all();

        return view('backend.whyus.index', compact('whyus'));
    }

    public function edit($id)
    {
        $whyus = WhyUs::findOrFail($id);

        return view('backend.whyus.edit', compact('whyus'));
    }

    public function update($id, Request $request)
    {
        $whyus = WhyUs::findOrFail($id);

        $whyus->update($request->all());

        return redirect('admin/whyus');
    }
}
