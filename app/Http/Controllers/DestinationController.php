<?php

namespace App\Http\Controllers;

use App\LocationDestination;
use Illuminate\Http\Request;

use App\Traits\Images;
use App\Destination;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class DestinationController extends Controller
{
    use Images;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $destinations = Destination::paginate(10);

        return view('backend.destination.index', compact('destinations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $location = LocationDestination::lists('location','id');

        return view('backend.destination.create', compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $image = Destination::create($request->except('image'));

        if($request->hasFile('image'))
        {
            list($filename, $upload) = $this->Image($request);

            Image::make($upload)->resize(350, 350)->save();

            $image->image = $filename;
            $image->save();
        }

        return redirect('admin/destination');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $location = LocationDestination::lists('location','id');
        $destination = Destination::findOrFail($id);

        return view('backend.destination.edit', compact('destination','location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $image = Destination::findOrFail($id);

        if ($request->hasFile('image'))
        {
            list($filename, $upload) = $this->Image($request);

            if($image->image)
            {
                $this->DeleteImage($image);
            }

            Image::make($upload)->resize(350, 350)->save();

            $image->image = $filename;
            $image->save();
        }

        if (!$image->update($request->except('image')))
        {
            return Redirect::back();
        }

        return redirect('admin/destination');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = Destination::findOrFail($id);

        if ($image->image)
        {
            $this->DeleteImage($image);
        }

        $image->delete();


        return redirect('admin/destination');
    }
}
