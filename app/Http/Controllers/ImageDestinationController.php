<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\Images;
use App\Destination;
use App\ImageDestination;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ImageDestinationController extends Controller
{
    use Images;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $images = ImageDestination::paginate(6);

        return view('backend.image.index', compact('images'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $destination = Destination::lists('title','id');

        return view('backend.image.create', compact('destination'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $image = ImageDestination::create($request->except('image'));

        if($request->hasFile('image'))
        {
            list($filename, $upload) = $this->Image($request);

            Image::make($upload)->resize(800, 400)->save();

            $image->image = $filename;
            $image->save();
        }

        return redirect('admin/image');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $destination = Destination::lists('title','id');
        $image = ImageDestination::findOrFail($id);

        return view('backend.image.edit', compact('destination','image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $image = ImageDestination::findOrFail($id);

        if ($request->hasFile('image'))
        {
            list($filename, $upload) = $this->Image($request);

            if($image->image)
            {
                $this->DeleteImage($image);
            }

            Image::make($upload)->resize(800, 400)->save();

            $image->image = $filename;
            $image->save();
        }

        if (!$image->update($request->except('image')))
        {
            return Redirect::back();
        }

        return redirect('admin/image');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = ImageDestination::findOrFail($id);

        if ($image->image)
        {
            $this->DeleteImage($image);
        }

        $image->delete();

        return redirect('admin/image');
    }
}
