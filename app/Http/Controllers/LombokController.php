<?php

namespace App\Http\Controllers;

use App\Lombok;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LombokController extends Controller
{
    public function index()
    {
        $lombok = Lombok::all();

        return view('backend.lombok.index', compact('lombok'));
    }

    public function edit($id)
    {
        $lombok = Lombok::findOrFail($id);

        return view('backend.lombok.edit', compact('lombok'));
    }

    public function update($id, Request $request)
    {
        $lombok = Lombok::findOrFail($id);

        $lombok->update($request->all());

        return redirect('admin/lombok');
    }
}
