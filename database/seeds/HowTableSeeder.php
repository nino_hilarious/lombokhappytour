<?php

use Illuminate\Database\Seeder;

class HowTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hows')->insert([
            'content' => str_random(10)
        ]);
    }
}
