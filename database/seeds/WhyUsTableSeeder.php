<?php

use Illuminate\Database\Seeder;

class WhyUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('why_uses')->insert([
           'content' => str_random(10)
        ]);
    }
}
