<?php

use Illuminate\Database\Seeder;

class LombokTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lomboks')->insert([
            'content' => str_random(10),
            'content_en' => str_random(10)
        ]);
    }
}
