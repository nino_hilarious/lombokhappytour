<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecomendReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recomend_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->integer('people');
            $table->date('date');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recomend_reservations');
    }
}
