<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleEnAndContentEnAtRecomendPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recomend_packages', function (Blueprint $table) {
            $table->string('title_en')->after('title');
            $table->text('content_en')->after('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recomend_packages', function (Blueprint $table) {
            $table->dropColumn('title_en');
            $table->dropColumn('content_en');
        });
    }
}
