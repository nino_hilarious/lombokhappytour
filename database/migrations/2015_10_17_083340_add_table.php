<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destinations', function (Blueprint $table) {
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('alt');
            $table->string('meta_title_en');
            $table->string('meta_description_en');
            $table->string('alt_en');
        });

        Schema::table('lomboks', function (Blueprint $table) {
            $table->string('meta_title');
            $table->string('meta_description');
            $table->text('content_en');
            $table->string('meta_title_en');
            $table->string('meta_description_en');
        });

        Schema::table('recomend_packages', function (Blueprint $table) {
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('alt');
            $table->string('meta_title_en');
            $table->string('meta_description_en');
            $table->string('alt_en');
        });

        Schema::table('why_uses', function (Blueprint $table) {
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_title_en');
            $table->string('meta_description_en');
        });

        Schema::table('hows', function (Blueprint $table) {
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_title_en');
            $table->string('meta_description_en');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_title_en');
            $table->string('meta_description_en');
        });

        Schema::table('image_destinations', function (Blueprint $table) {
            $table->string('alt');
            $table->string('alt_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('destinations', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('alt');
            $table->dropColumn('meta_title_en');
            $table->dropColumn('meta_description_en');
            $table->dropColumn('alt_en');
        });

        Schema::table('recomend_packages', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('alt');
            $table->dropColumn('meta_title_en');
            $table->dropColumn('meta_description_en');
            $table->dropColumn('alt_en');
        });

        Schema::table('lomboks', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('content_en');
        });

        Schema::table('why_uses', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_title_en');
            $table->dropColumn('meta_description_en');
        });

        Schema::table('hows', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_title_en');
            $table->dropColumn('meta_description_en');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_title_en');
            $table->dropColumn('meta_description_en');
        });

        Schema::table('image_destinations', function (Blueprint $table) {
            $table->dropColumn('alt');
            $table->dropColumn('alt_en');
        });
    }
}
