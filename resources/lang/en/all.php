<?php

return [
  'news' => 'News',
  'home' => 'Home',
  'what is lombok' => 'What is Lombok',
  'destination' => 'Destination',
  'recomendation package' => 'Our Recomended Package',
  'reservation' => 'How to Reservation',
  'why us' => 'Why Us',
  'create own tour' => 'Create Own Tour',
  'book now' => 'Book Now',
  'language' => 'Language',
  'header 1' => 'You can create your own tour!',
  'header 2' => 'Our goal is to make your holiday to be enjoyable!',
  'header 3' => 'Free to enjoy the beauty of Lombok :)',
  'recomended tour' => 'Recommended Tour',
  'syarat' => 'Terms & Conditions',
  'keterangan' => 'Explanation',
  'ket own 1' => 'Fill out the required form according to the instructions.',
  'ket own 2' => 'Fill out the form with valid data, so we can call you back.',
  'ket own 3' => 'We will contact you again to the details of the activities and the price of the tour you want.',
  'ket own 4' => 'For a place to stay we will adjust to destinations purpose and the funds you have.',
  'ket own 5' => 'For the "Date" is the date you arrive in Lombok.',
  'ket own 6' => 'If there is something you want to convey, please fill in notes.',
  'place own 1' => 'Enter Your Name',
  'place own 2' => 'Enter Your E-mail',
  'place own 3' => 'Enter Your Address',
  'place own 4' => 'Enter Your Phone Number',
  'place own 5' => 'Choose Your Destination',
  'place own 6' => 'Enter Number of People Who Come Vacationing',
  'place own 7' => 'Enter How Many Days Of Your Vacation',
  'place own 8' => 'Your Message',
];