<!-- Meta_title Form Input -->
<div class="form-group">
    {!!	Form::label('meta_title', 'Meta Title : ') !!}
    {!!	Form::text('meta_title', null, ['class'=>'form-control']) !!}
</div>

<!-- Meta_description Form Input -->
<div class="form-group">
    {!! Form::label('meta_description', 'Meta Description :') !!}
    {!! Form::textarea('meta_description', null, ['class'=>'form-control', 'placeholder'=>'']) !!}
</div>

<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class'=>'form-control', 'id'=>'summernote']) !!}
</div>

<!-- Meta_title_en Form Input -->
<div class="form-group">
    {!!	Form::label('meta_title_en', 'Meta Title in English : ') !!}
    {!!	Form::text('meta_title_en', null, ['class'=>'form-control']) !!}
</div>

<!-- Meta_description_en Form Input -->
<div class="form-group">
    {!! Form::label('meta_description_en', 'Meta Description in English :') !!}
    {!! Form::textarea('meta_description_en', null, ['class'=>'form-control', 'placeholder'=>'']) !!}
</div>

<div class="form-group">
    {!! Form::label('content_en', 'Content in English:') !!}
    {!! Form::textarea('content_en', null, ['class'=>'form-control', 'id'=>'summernote2']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitbutton, ['class'=>'btn btn-primary form-control']) !!}
</div>