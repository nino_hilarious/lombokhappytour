<div class="form-group">
    {!! Form::label('destination', 'Destination') !!}
    {!! Form::select('destination_id', $destination ,null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitbutton, ['class'=>'btn btn-primary form-control']) !!}
</div>