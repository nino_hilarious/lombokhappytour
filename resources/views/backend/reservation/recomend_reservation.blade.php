@extends('backend.layout')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Recomend Reservation
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Recomend Reservation</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Recomend Reservation</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table" style="text-align: center">
                                        <tr>
                                            <th style="width: 30%; text-align: center">Waktu Kirim Reservasi</th>
                                            <th style="width: 25%; text-align: center">Name</th>
                                            <th style="width: 30%; text-align: center">Paket</th>
                                            <th style="width: 7.5%"></th>
                                            <th style="width: 7.5%"></th>
                                        </tr>
                                        @foreach($all as $tampil)
                                            <tr>
                                                <td class="danger">{!! $tampil->created_at !!}</td>
                                                <td class="active">{!! $tampil->name !!}</td>
                                                <td class="success">{!! $tampil->package !!}</td>
                                                <td class="primary">
                                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#{{$tampil->id}}">
                                                        Show Detail
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="{{$tampil->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <b>Detail Pemesan</b>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <br>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <b>Paket Pilihan</b> : {!! $tampil->package !!} <br>
                                                                    <b>Nama</b> : {!! $tampil->name !!} <br>
                                                                    <b>E-mail</b> : {!! $tampil->email !!} <br>
                                                                    <b>Phone</b> : {!! $tampil->phone !!} <br>
                                                                    <b>Alamat</b> : {!! $tampil->address !!} <br>
                                                                    <b>Jumlah Peserta</b> : {!! $tampil->people !!} <br>
                                                                    <b>Tanggal Berlibur</b> : {!! $tampil->date !!} <br>
                                                                    <b>Catatan</b> : {!! $tampil->message !!} <br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="warning">
                                                {!! Form::open(['method'=>'DELETE' ,'action' => ['RecomendReservationController@destroy', $tampil->id]]) !!}
                                                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                                {!! Form::close() !!}
                                                <td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    {!! $all->render() !!}
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div><!-- ./box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection