<div class="form-group">
    {!! Form::label('location', 'Nama Lokasi :') !!}
    {!! Form::text('location', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('location_en', 'Name Of Location :') !!}
    {!! Form::text('location_en', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitbutton, ['class'=>'btn btn-primary form-control']) !!}
</div>