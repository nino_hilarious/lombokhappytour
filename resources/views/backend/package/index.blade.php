@extends('backend.layout')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Recomend Package
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Recomend Package</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Recomend Package</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table" style="text-align: center">
                                        <tr>
                                            <th style="width: 45%; text-align: center">Title</th>
                                            <th style="width: 40%; text-align: center">Content</th>
                                            <th style="width: 7.5%"></th>
                                            <th style="width: 7.5%"></th>
                                        </tr>
                                        @foreach($packages as $package)
                                            <tr>
                                                <td class="active">{{$package->title}}</td>
                                                <td class="success">{!! $package->content !!}</td>
                                                <td class="warning">
                                                    <a href="{{action('RecomendPackageController@edit', $package->id)}}" class="btn btn-primary" role="button">Edit</a>
                                                </td>
                                                <td class="warning">
                                                {!! Form::open(['method'=>'DELETE' ,'action' => ['RecomendPackageController@destroy', $package->id]]) !!}
                                                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                                {!! Form::close() !!}
                                                <td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <a href="{{action('RecomendPackageController@create')}}" class="btn btn-primary" role="button">Create</a>
                            </div>
                        </div><!-- /.row -->
                    </div><!-- ./box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection