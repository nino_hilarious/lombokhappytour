@extends('backend.layout')

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Destination
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Destination</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Destination</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table" style="text-align: center">
                                        <tr>
                                            <th style="width: 35%; text-align: center">Title</th>
                                            <th style="width: 30%; text-align: center">Image</th>
                                            <th style="width: 20%; text-align: center">Location</th>
                                            <th style="width: 7.5%"></th>
                                            <th style="width: 7.5%"></th>
                                        </tr>
                                        @foreach($destinations as $destination)
                                            <tr>
                                                <td class="active">{{$destination->title}}</td>
                                                <td class="success">
                                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#{{$destination->id}}">
                                                        Show Image
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="{{$destination->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <br>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <center>{!! Html::image(asset('img/'. $destination->image), null, ['class' => 'img img-thumbnail']) !!}</center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="primary">
                                                    {{ $destination->location->location }}
                                                </td>
                                                <td class="warning">
                                                    <a href="{{action('DestinationController@edit', $destination->id)}}" class="btn btn-primary" role="button">Edit</a>
                                                </td>
                                                <td class="warning">
                                                {!! Form::open(['method'=>'DELETE' ,'action' => ['DestinationController@destroy', $destination->id]]) !!}
                                                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                                {!! Form::close() !!}
                                                <td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-8">
                                <a href="{{action('DestinationController@create')}}" class="btn btn-primary" role="button">Create</a>
                            </div>
                            <div class="col-md-4" style="text-align: right">
                                {!! $destinations->render() !!}
                            </div>
                        </div>
                    </div><!-- ./box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection