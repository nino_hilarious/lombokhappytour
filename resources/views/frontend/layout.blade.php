<!DOCTYPE html>
<html lang="@if(LaravelLocalization::getCurrentLocale() === 'id')id @else en @endif">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Arino Tours - Tour & Travel Lombok">
    <link rel="shortcut icon" type="image/png" href="{{asset('img/favicon.png')}}"/>
    <meta name="copyright" content="Arino Tours">
    <!-- Bing -->
    <meta name="msvalidate.01" content="BC45FDEBDECC960B93F2ACDF3E47CE43" />

    @yield('metadescription')

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-61419341-6', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/Frontend/frontend.css')}}" rel="stylesheet">
    <link href="{{asset('css/Frontend/rotate.css')}}" rel="stylesheet">
    <link href="{{asset('css/Plugins/flag.css')}}" rel="stylesheet">
    <link href="{{asset('css/Plugins/slider.css')}}" rel="stylesheet">
    <!-- Plugin Css from CDN -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
    <!-- End Custom Fonts -->

</head>

<body>
<div class="top-menu">
    <div class="container" itemscope>
        <div class="col-md-2" style="text-align: center;">
            <i><a href="https://www.facebook.com/arinotours" class="fa fa-facebook-official fa-2x"></a></i>
            <i><a href="https://twitter.com/arino_tours" class="fa fa-twitter fa-2x"></a></i>
            <i><a href="https://instagram.com/arinolomboktours/" class="fa fa-instagram fa-2x"></a></i>
        </div>
        <div class="col-md-8" style="text-align: center;">
            <i class="fa fa-mobile fa-2x" itemprop="mobile"></i> (+62)-81-805-741-309 ||
            <i class="fa fa-phone fa-2x" itemprop="phone"></i> (+62)370 - 6172808 ||
            <i class="fa fa-weixin fa-2x" itemprop="bbm"></i> BBM : 523F8E6E ||
            <i class="fa fa-envelope-o fa-2x" itemprop="e-mail"></i> arinotours@gmail.com
        </div>

        <div class="col-md-2" style="text-align: center;">
            {!! trans('all.language') !!} :
            <a rel="canonical" href="{{url('en/pages')}}" class="flag-icon flag-icon-en flag-icon-squared"></a>
            <a rel="canonical" href="{{url('id/pages')}}" class="flag-icon flag-icon-id flag-icon-squared"></a>
        </div>

    </div>
</div>
<div class="jumbotron" itemscope>
    <div class="container">
        <center>
            <h2 class="font-header bounce animated textforrotate">
                    <span class="rotate textrotate">
                        {!! trans('all.header 1') !!} <i class="fa fa-bus"></i>,
                        {!! trans('all.header 2') !!},
                        {!! trans('all.header 3') !!}
                    </span>
                <br>
                <br>
                <p>
                    <a rel="canonical" class="btn btn-danger btn-lg animated swing" href="{{url('pages/how-to-reservation')}}" role="button">
                        <b> {!! trans('all.book now') !!}..! <i class="fa fa-calendar"></i> </b>
                    </a>
                </p>
            </h2>
        </center>
    </div>
</div>

<div class="container" itemscope>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="{{Request::path() == 'pages' ? 'active':''}}"><a rel="canonical" href="{{url('pages')}}">{!! trans('all.home') !!} <span class="sr-only">(current)</span></a></li>
                    <li class="{{Request::path() == 'pages/destination' ? 'active':''}}"><a rel="canonical" href="{{url('pages/destination')}}">{!! trans('all.destination') !!}</a></li>
                    <li class="{{Request::path() == 'pages/recomend-package' ? 'active':''}}"><a rel="canonical" href="{{url('pages/recomend-package')}}">{!! trans('all.recomendation package') !!}</a></li>
                    <li class="{{Request::path() == 'pages/how-to-reservation' ? 'active':''}}"><a rel="canonical" href="{{url('pages/how-to-reservation')}}">{!! trans('all.reservation') !!} <span class="badge">?</span></a></li>
                    <li class="{{Request::path() == 'pages/why-us' ? 'active':''}}"><a rel="canonical" href="{{url('pages/why-us')}}">{!! trans('all.why us') !!} <span class="badge">?</span></a></li>
                    <li class="{{Request::path() == 'pages/news' ? 'active':''}}"><a rel="canonical" href="{{url('pages/news')}}">{!! trans('all.news') !!}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a rel="canonical" class="btn btn-default" href="{{url('pages/own-reservation')}}" role="button">
                            <b> {!! trans('all.create own tour') !!} </b>
                        </a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <br>
    @yield('content')

</div>
<br>
<br>

<footer class="footer" itemscope="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p itemprop="copyright">&copy; 2015 Arino Tours by CV.TechnoWorld</p>
            </div>
            <div class="col-md-4">
                <p>
                    {!! trans('all.syarat') !!} | About Us | Career
                </p>
            </div>
        </div>
    </div>
</footer>

<!--================= JAVASCRIPT FILE ====================-->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/Plugins/responsiveslides.min.js')}}"></script>
    <script src="{{asset('js/Frontend/jquery.rotate.js')}}"></script>
    <!--Plugin JS from CDN -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <!--Start of Zopim Live Chat Script-->
    <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
                _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="//v2.zopim.com/?2zaapRky17fgcBuisUuEvplxG8TBBEmx";z.t=+new Date;$.
                    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <!--End of Zopim Live Chat Script-->

    <!-- Facebook SDK -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.4&appId=1021782697840763";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- End Facebook SDK -->

    <!-- Custom JS -->
    <script>
        $(document).ready(function(){
            $(".rotate").textrotator({
                animation: "fade",
                speed: 3000
            });
            $("#slider2").responsiveSlides({
                auto: true,
                pager: true,
                speed: 300
            });
            $('div.alert-important').delay(7000).slideUp(400);
            $('#destination').select2({
                placeholder: '{!! trans('all.place own 5') !!}'
            });
        });
    </script>
<!--================= End JAVASCRIPT FILE ================-->
</body>
</html>
