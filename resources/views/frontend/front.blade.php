<!DOCTYPE html>
<html>
<head>
    <meta property="og:image" content="https://lh3.googleusercontent.com/umfiQb2yW2kf70fRynjVAUIIEYiD-AU7hjvji3Uo6QjNWdBOy-qJtCuD_z6vVtt4BtpQYWtLueTf1wk=w1338-h527" />
    <meta name="description" content="Arino Tours is a cheapest lombok tour and travel services in Lombok. Here, you can create your own tour package and we will make your holiday in Lombok to be enjoyable!">
    <meta name="author" content="Arino Tours - Tour & Travel Lombok ">
    <link rel="shortcut icon" type="image/png" href="{{asset('img/favicon.png')}}"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="copyright" content="Arino Tours">
    <!-- Bing -->
    <meta name="msvalidate.01" content="BC45FDEBDECC960B93F2ACDF3E47CE43" />

    <title> Cheapest Lombok Tour - Holiday in Lombok | Arino Tours</title>

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-61419341-6', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/Frontend/frontpage.css')}}" rel="stylesheet">
    <link href="{{asset('css/Frontend/rotate.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
    <!-- End Custom Fonts -->

</head>

<body>

    <section class="front-section">
        <div class="container">
            <div class="font-section">
                <h1 class="font-header bounce animated textforrotate" itemscope>
                    <header class="rotate textrotate" itemprop="header">
                        In Here You Can Create Own Tour Package,
                        Are You Ready to Holiday in Lombok?,
                        You Need Refreshing?,
                        Want to Know About Lombok?
                    </header>
                </h1>
                <br>
                <a rel="canonical" class="btn btn-success btn-lg animated tada infinite" href="{{url('pages')}}" role="button"><b>Let's Holiday in Lombok..!!</b></a>
            </div>
        </div>
    </section>
    <!--================= JAVASCRIPT FILE ====================-->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/Frontend/jquery.rotate.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(".rotate").textrotator({
                animation: "fade",
                speed: 3000
            });
        });
    </script>
    <!--================= End JAVASCRIPT FILE ================-->

</body>
</html>
