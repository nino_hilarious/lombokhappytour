@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="Reservasi dari Rekomendasi Paket Tour Kami">
    @else
        <meta name="description" content="Booking Package Tour What We Recommend">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> Pemesanan Paket Tour Yang Kami Rekomendasikan </title>
    @else
        <title> Booking Package Tour What We Recommend </title>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-7" itemscope>
            <center><h1 itemprop="header">Recomend Tour Reservation Form</h1></center>
            <br />
            @if($errors->any())
                <ul class="bg-danger">
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            @endif
            @if(session()->has('status'))
                <div class="alert alert-success alert-important">
                    <center>{!! session('status') !!}</center>
                </div>
            @endif

            <br>
            {!! Form::open(['action' => 'RecomendReservationController@store']) !!}
                <div class="form-group">
                    {!! Form::label('package', 'Package :') !!}
                    {!! Form::select('package', $recomend ,null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Your Name :') !!}
                    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 1')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Your E-mail :') !!}
                    {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 2')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address', 'Your Address :') !!}
                    {!! Form::text('address', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 3')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Your Phone :') !!}
                    {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 4')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('people', 'Number of People :') !!}
                    {!! Form::text('people', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 6')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('date', 'Date :') !!}
                    {!! Form::date('date' , \Carbon\Carbon::now()->format('Y-m-d'), ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('message', 'Your Message :') !!}
                    {!! Form::textarea('message', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 8')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Send', ['class'=>'btn btn-primary form-control']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-5" itemscope>
            <br>
            <center><b>{!! trans('all.keterangan') !!} :</b></center>
            <br>
            <ul>
                <li>{!! trans('all.ket own 1') !!}</li>
                <li>{!! trans('all.ket own 2') !!}</li>
                <li>{!! trans('all.ket own 5') !!}</li>
                <li>{!! trans('all.ket own 6') !!}</li>
            </ul>
        </div>
    </div>
@stop
