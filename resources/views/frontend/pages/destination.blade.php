@extends('frontend.layout')

@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="Destinasi - destinasi wisata yang ada di Pulau Lombok. Seperti Gili Trawangan, Gunung Rinjani, Pantai - pantai di bagian selatan Lombok, dan lain sebagainya.">
    @else
        <meta name="description" content="Tourist destinations on the island of Lombok. Such as Gili Trawangan, Mount Rinjani, Various beach in southern Lombok, etc.">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> Destinasi Wisata di Pulau Lombok </title>
    @else
        <title> Tour Destination in Lombok </title>
    @endif
@endsection

@section('content')
    @include('frontend.pages._filter')

    <div class="row">
        @foreach($destinations as $destination)
            <div class="col-md-4" itemscope>
                <div class="panel panel-primary">
                    <div class="panel-heading" itemprop="header destination">
                        @if(LaravelLocalization::getCurrentLocale() === 'id')
                            <h3 class="panel-title">{!! $destination->title !!}</h3>
                        @else
                            <h3 class="panel-title">{!! $destination->title_en !!}</h3>
                        @endif
                    </div>
                    <div class="panel-body" itemprop="image">
                        <center><img src="{{asset('img/'.$destination->image)}}" class="img-responsive img-thumbnail" alt="{!! $destination->alt !!}"></center>
                    </div>
                    <div class="panel-footer" itemprop="view details">
                        <p>
                        <center>
                            <a rel="canonical" class="btn btn-success" href="{{url('pages/destination', $destination->slug)}}" role="button">View Details &raquo;</a>
                        </center>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row" itemscope="">
        <span itemprop="pagination"> {!! $destinations->render() !!} </span>
    </div>

    <!-- Js -->
    <script>

    </script>
@stop