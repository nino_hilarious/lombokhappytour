@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="{!! $destination->meta_description !!}">
    @else
        <meta name="description" content="{!! $destination->meta_description_en !!}">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> {!! $destination->meta_title !!} </title>
    @else
        <title> {!! $destination->meta_title_en !!} </title>
    @endif
@endsection

<!-- JS -->
<div id="fb-root"></div>
<script>
    $(document).ready(function($){
        var url = window.location;
        $('.fb-share-button').attr('data-href', url);
    });
</script>

@section('content')
    <div class="row">
        <div class="col-md-9" itemscope>
            @if(LaravelLocalization::getCurrentLocale() === 'id')
                <span itemprop="title"> <center><h1>{!! $destination->title !!}</h1></center> </span>
            @else
                <span itemprop="title"> <center><h1>{!! $destination->title_en !!}</h1></center> </span>
            @endif
            <br />
            <ul class="rslides" id="slider2">
                @foreach($destination->imageDestination as $key => $value)
                    <li>
                        <center><img itemprop="image" src="{{asset('img/'.$value->image)}}" class="img-responsive img-thumbnail" alt="{!! $destination->alt !!}"></center>
                    </li>
                @endforeach
            </ul>

            <br />

            <p itemprop="content">
                @if(LaravelLocalization::getCurrentLocale() === 'id')
                    {!! $destination->content !!}
                @else
                    {!! $destination->content_en !!}
                @endif
            </p>

            <br>
            <p>
                <div class="fb-share-button" data-layout="button_count"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-via="nino_hilarious" data-hashtags="holidayinlombok,lomboktour,arinotours">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

            </p>

            <br>
            <p style="text-align: center" itemprop="reservation">
                <a rel="canonical" class="btn btn-danger" href="{{url('pages/own-reservation')}}" role="button">{!! trans('all.create own tour') !!}</a>
            </p>

                <br>
                <center><div class="fb-comments" data-href="{!! url('pages/destination/', $destination->id) !!}" data-width="100%" data-numposts="5"></div></center>
                <br>
        </div>
    </div>
@stop
