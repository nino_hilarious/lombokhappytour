@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="{!! $how->meta_description !!}">
    @else
        <meta name="description" content="{!! $how->meta_description_en !!}">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> {!! $how->meta_title !!} </title>
    @else
        <title> {!! $how->meta_title_en !!} </title>
    @endif
@endsection

@section('content')
    <div class="row">
            <div class="col-md-9" itemscope>
                @if(LaravelLocalization::getCurrentLocale() === 'id')
                    <span itemprop="content"> {!! $how->content !!} </span>
                @else
                    <span itemprop="content"> {!! $how->content_en !!} </span>
                @endif
                <br><br><br>
                <center>
                <a rel="canonical" class="btn btn-primary" href="{{url('pages/own-reservation')}}" role="button">
                    <b> {!! trans('all.create own tour') !!} </b>
                </a>
                <a rel="canonical" class="btn btn-warning" href="{{url('pages/recomend-reservation')}}" role="button">
                    <b> {!! trans('all.recomended tour') !!} </b>
                </a>
                </center>
            </div>
    </div>
@stop
