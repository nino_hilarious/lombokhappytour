@extends('frontend.layout')

@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="Berita tentang pariwisata Lombok, Gili Trawangan, dan sekitarnya oleh Arino Tours">
    @else
        <meta name="description" content="News about tourism in Lombok">
    @endif


    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> Berita Terhangat Tentang Pariwisata Lombok, Gili Trawangan dan Pariwisata Lainnya. </title>
    @else
        <title> Hottest News About Tourism Lombok, Gili Trawangan and Tourism Others </title>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="container">
            <div class="col-md-8">
                @foreach($news as $tampil)
                    @if(LaravelLocalization::getCurrentLocale() === 'id')
                    <div class="row">
                        <div class="col-md-12 white">
                            <h2 style="font-family: Vollkron; font-weight: bold">{!! $tampil->title !!}</h2>
                            <p style="font-family: Vollkron; font-weight: bold">{!! substr($tampil->content, 0,800) !!} ..</p>
                            <p class="publish"> Publish at : {!! $tampil->created_at->diffForHumans() !!}</p>
                            <p><a class="btn btn-default" href="{{action('FrontendController@showNews',[$tampil->slug])}}" role="button">Read More &raquo;</a></p>
                        </div>
                    </div>
                    <br>
                    <hr>
                    @else
                        <div class="row">
                            <div class="col-md-12 white">
                                <h2 style="font-family: Vollkron; font-weight: bold">{!! $tampil->title_en !!}</h2>
                                <p style="font-family: Vollkron; font-weight: bold">{!! substr($tampil->content_en, 0,800) !!} ..</p>
                                <p class="publish"> Publish at : {!! $tampil->created_at->diffForHumans() !!}</p>
                                <p><a class="btn btn-default" href="{{action('FrontendController@showNews',[$tampil->slug])}}" role="button">Read More &raquo;</a></p>
                            </div>
                        </div>
                        <br>
                        <hr>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    </div>
@stop
