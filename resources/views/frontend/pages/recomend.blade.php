@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="Rekomendasi Paket Tour ke Lombok. Tour Murah Mengelilingi 3 Gili Populer di Lombok, Rinjani Trekking, dan lainnya.">
    @else
        <meta name="description" content="Recommended Package Tour to Lombok. Cheap Tour Surrounding 3 Popular Gili Lombok, Rinjani Trekking, and etc.">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> Rekomendasi Paket Tour ke Lombok </title>
    @else
        <title> Recomended Tour Package to Lombok </title>
    @endif
@endsection

@section('content')
    <div class="row">
        @foreach($recomends as $recomend)
            <div class="col-md-4" itemscope>
                <div class="panel panel-primary">
                    <div class="panel-heading" itemprop="title">
                        @if(LaravelLocalization::getCurrentLocale() === 'id')
                            <h3 class="panel-title">{!! $recomend->title !!}</h3>
                        @else
                            <h3 class="panel-title">{!! $recomend->title_en !!}</h3>
                        @endif
                    </div>
                    <div class="panel-body" itemprop="image">
                        <center><img src="{{asset('img/'.$recomend->image)}}" class="img-responsive img-thumbnail" alt="Responsive image"></center>
                    </div>
                    <div class="panel-footer" itemprop="details">
                        <p>
                        <center>
                            <a class="btn btn-success" href="{{url('pages/recomend-package', $recomend->slug)}}" role="button">View Details &raquo;</a>
                        </center>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
