@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="Buat Paket Tour Anda Sendiri">
    @else
        <meta name="description" content="Create Your Own Tour Package">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> Buat Paket Tour Anda Sendiri </title>
    @else
        <title> Create Your Own Tour Package </title>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-7" itemscope>
            <center><h1 itemprop="title">Own Tour Reservation Form</h1></center>
            <br />
            @if($errors->any())
                <ul class="bg-danger">
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            @endif
            @if(session()->has('status'))
                <div class="alert alert-success alert-important">
                    <center>{!! session('status') !!}</center>
                </div>
            @endif

            <br>
            {!! Form::open(['action'=>'OwnReservationController@store']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Your Name :') !!}
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 1')]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Your E-mail :') !!}
                {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 2')]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('address', 'Your Address :') !!}
                {!! Form::text('address', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 3')]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('phone', 'Your Phone :') !!}
                {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 4')]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('destination', 'Destination :') !!}
                {!! Form::select('destination[]', $destination ,null, ['id'=>'destination', 'class'=>'form-control', 'multiple']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('people', 'Number of People :') !!}
                {!! Form::text('people', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 6')]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('day', 'How Many Days :') !!}
                {!! Form::text('day', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 7')]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('hotel', 'Place to Stay :') !!}
                {!! Form::select('hotel', ['Kos Harian'=>'Kos Harian', 'Hotel Bintang 2'=>'Hotel &#xf005; &#xf005;', 'Hotel Bintang 3'=>'Hotel &#xf005; &#xf005; &#xf005;', 'Hotel Bintang 4'=>'Hotel &#xf005; &#xf005; &#xf005; &#xf005;',] ,null, ['class'=>'form-control', 'style'=>"font-family: 'FontAwesome', Arial;"]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('date', 'Date :') !!}
                {!! Form::date('date' , \Carbon\Carbon::now()->format('Y-m-d'), ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('message', 'Your Message :') !!}
                {!! Form::textarea('message', null, ['class'=>'form-control', 'placeholder'=>trans('all.place own 8')]) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Send', ['class'=>'btn btn-primary form-control']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-5" itemscope>
            <br>
            <center><b>{!! trans('all.keterangan') !!} :</b></center>
            <br>
            <ul>
                <li>{!! trans('all.ket own 1') !!}</li>
                <li>{!! trans('all.ket own 2') !!}</li>
                <li>{!! trans('all.ket own 3') !!}</li>
                <li>{!! trans('all.ket own 4') !!}</li>
                <li>{!! trans('all.ket own 5') !!}</li>
                <li>{!! trans('all.ket own 6') !!}</li>
            </ul>
            <br>
        </div>
    </div>
@stop
