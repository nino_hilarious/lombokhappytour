@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="Destinasi Wisata di {!! $location->location !!}, Buat Paket Anda Sendiri Bersama Kami Arino Tours">
    @else
        <meta name="description" content="Tour Destination in {!! $location->location_en !!}, Create Your Own Tour Package with Us Arino Tours">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> Destinasi Wisata di {!! $location->location !!} </title>
    @else
        <title> Tour Destination in {!! $location->location_en !!} </title>
    @endif
@endsection

@section('content')

    @include('frontend.pages._filter')

    <div class="row">
        @foreach($location->destination as $key => $value)
            <div class="col-md-4" itemscope>
                <div class="panel panel-primary">
                    <div class="panel-heading" itemprop="title">
                        <h3 class="panel-title">{!! $value->title !!}</h3>
                    </div>
                    <div class="panel-body" itemprop="image">
                        <center><img src="{{asset('img/'.$value->image)}}" class="img-responsive img-thumbnail" alt="{!! $value->title !!}"></center>
                    </div>
                    <div class="panel-footer" itemprop="details">
                        <p>
                        <center>
                            <a rel="canonical" class="btn btn-success" href="{{url('pages/destination', $value->slug)}}" role="button">View Details &raquo;</a>
                        </center>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
