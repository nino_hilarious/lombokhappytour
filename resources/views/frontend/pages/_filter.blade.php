<div class="row">
    <div class="container" itemscope>
        Filter by Location :
        <a rel="canonical" class="btn btn-success" href="{{url('pages/destination')}}" role="button">All</a>
        @foreach($locations as $location)
            @if(LaravelLocalization::getCurrentLocale() === 'id')
                <span itemprop="lokasi"><a rel="canonical" class="btn btn-success" href="{{url('pages/location', $location->slug)}}" role="button">{{ $location->location }}</a></span>
            @else
                <span itemprop="location"> <a rel="canonical" class="btn btn-success" href="{{url('pages/location', $location->slug)}}" role="button">{{ $location->location_en }}</a>  </span>
            @endif
        @endforeach
    </div>
</div>
<br><br>