@extends('frontend.layout')

@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="{!! $lombok->meta_description !!}">
    @else
        <meta name="description" content="{!! $lombok->meta_description_en !!}">
    @endif


    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> {!! $lombok->meta_title !!} </title>
    @else
        <title> {!! $lombok->meta_title_en !!} </title>
    @endif
@endsection

@section('content')
        <div class="row">
            <div class="container">
                <div class="col-md-12" itemscope="content">
                    @if(LaravelLocalization::getCurrentLocale() === 'id')
                        {!! $lombok->content !!}
                    @else
                        {!! $lombok->content_en !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
