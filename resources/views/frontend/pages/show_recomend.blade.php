@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="{!! strip_tags($recomend->meta_description) !!}">
    @else
        <meta name="description" content="{!! strip_tags($recomend->meta_description_en) !!}">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> {!! $recomend->meta_title !!} </title>
    @else
        <title> {!! $recomend->meta_title_en !!} </title>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            @if(LaravelLocalization::getCurrentLocale() === 'id')
                <center><h1>{!! $recomend->title !!}</h1></center>
            @else
                <center><h1>{!! $recomend->title_en !!}</h1></center>
            @endif
            <br />
            <p>
                @if(LaravelLocalization::getCurrentLocale() === 'id')
                    {!! $recomend->content !!}
                @else
                    {!! $recomend->content_en !!}
                @endif
            </p>
            <br>
            <p style="text-align: center">
                <a class="btn btn-danger" href="{{url('pages/recomend-reservation')}}" role="button">{!! trans('all.book now') !!}</a>
            </p>
        </div>
    </div>
@stop
