@extends('frontend.layout')
@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="{!! $whyus->meta_description !!}">
    @else
        <meta name="description" content="{!! $whyus->meta_description_en !!}">
    @endif

    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> {!! $whyus->meta_title !!} </title>
    @else
        <title> {!! $whyus->meta_title_en !!} </title>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12" itemscope>
            @if(LaravelLocalization::getCurrentLocale() === 'id')
                <span itemprop="content"> {!! $whyus->content !!} </span>
            @else
                <span itemprop="content"> {!! $whyus->content_en !!} </span>
            @endif
        </div>
    </div>
@stop
