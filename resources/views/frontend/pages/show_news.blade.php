@extends('frontend.layout')

@section('metadescription')
    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <meta name="description" content="{!! strip_tags($news->meta_description) !!}">
    @else
        <meta name="description" content="{!! strip_tags($news->meta_description_en) !!}">
    @endif


    @if(LaravelLocalization::getCurrentLocale() === 'id')
        <title> {!! $news->title !!} </title>
    @else
        <title> {!! $news->title_en !!} </title>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                @if(LaravelLocalization::getCurrentLocale() === 'id')
                    <div class="row">
                        <div class="col-md-12 white">
                            <h2 style="font-family: Vollkron; font-weight: bold">{!! $news->title !!}</h2>
                            <br>
                            <br>

                            <p>{!!  $news->content !!}</p>
                            <br>
                            <p>
                            <div class="fb-share-button" data-layout="button_count"></div>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-via="nino_hilarious" data-hashtags="holidayinlombok,lomboktour,arinotours">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </p>
                            <br>
                            <p class="publish"> Publish at : {!! $news->created_at->diffForHumans() !!}</p>
                            <br>
                            <center><div class="fb-comments" data-href="{!! url('pages/news/', $news->id) !!}" data-width="100%" data-numposts="5"></div></center>
                            <br>
                        </div>
                    </div>
                    <br>
                    <p><a class="btn btn-default" href="{{URL::to('pages/news')}}" role="button">&ShortLeftArrow; Back</a></p>
                @else
                    <div class="row">
                        <div class="col-md-12 white">
                            <h2 style="font-family: Vollkron; font-weight: bold">{!! $news->title_en !!}</h2>
                            <br>
                            <br>

                            <p>{!!  $news->content_en !!}</p>
                            <br>
                            <p>
                            <div class="fb-share-button" data-layout="button_count"></div>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-via="nino_hilarious" data-hashtags="holidayinlombok,lomboktour,arinotours">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

                            </p>
                            <p class="publish"> Publish at : {!! $news->created_at->diffForHumans() !!}</p>
                            <br>
                            <center><div class="fb-comments" data-href="{!! url('pages/news/', $news->id) !!}" data-width="100%" data-numposts="5"></div></center>
                            <br>
                        </div>
                    </div>
                    <br>
                    <p><a class="btn btn-default" href="{{URL::to('pages/news')}}" role="button">&ShortLeftArrow; Back</a></p>
                @endif
            </div>
        </div>
    </div>
    </div>
@stop
