<!DOCTYPE HTML>
<html>
<head>
    <title>Lombok Happy Tour</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>

<body>

<div class="container">
    <br>
    <br>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        @yield('content')
    </div>
    <div class="col-md-4"></div>
</div>

<!-- JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>

</html>