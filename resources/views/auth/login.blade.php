@extends('auth.layout')

@section('content')

    <h2 style="text-align: center">Login Page</h2>
    <hr>
    <br>

    <!-- Login Page -->
    {!! Form::open(['url'=>'auth/login']) !!}

        <div class="form-group">
            {!! Form::label('email', 'E-email') !!}
            {!! Form::text('email', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('remember', 'Remember Me') !!} Remember Me
        </div>

        <div class="form-group">
            {!! Form::submit('Login', ['class'=>'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}
@stop