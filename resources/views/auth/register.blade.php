@extends('auth.layout')

@section('content')

    <h2 style="text-align: center">Register Page</h2>
    <hr>
    <br>

    <!-- Login Page -->
    {!! Form::open(['url'=>'auth/register']) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::text('email', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password_confirmation', 'Password') !!}
            {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Register', ['class'=>'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}
@stop