var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
        //copy file css
    mix.copy(
            "vendor/bower_components/bootstrap/dist/css/bootstrap.css",
            "resources/assets/css/bootstrap.css"
        )
        .copy(
            "vendor/bower_components/font-awesome/css/font-awesome.css",
            "resources/assets/css/font-awesome.css"
        )
        .copy(
            "vendor/bower_components/animation.css/animate.css",
            "resources/assets/css/animate.css"
        )
        .copy(
            "vendor/bower_components/admin-lte/dist/css/AdminLTE.css",
            "resources/assets/css/AdminLTE.css"
        )
        .copy(
            "vendor/bower_components/admin-lte/dist/css/skins/_all-skins.css",
            "resources/assets/css/_all-skins.css"
        )
        //copy file js
        .copy(
            "vendor/bower_components/bootstrap/dist/js/bootstrap.js",
            "public/js/bootstrap.js"
        )
        .copy(
            "vendor/bower_components/jquery/dist/jquery.js",
            "public/js/jquery.js"
        )
        .copy(
            "vendor/bower_components/admin-lte/dist/js/app.js",
            "public/js/AdminLTE/app.js"
        )
        .copy(
            "vendor/bower_components/admin-lte/plugins/slimScroll/jquery.slimscroll.js",
            "public/js/Plugins/jquery.slimscroll.js"
        )
        .copy(
            "vendor/bower_components/admin-lte/dist/js/pages/dashboard2.js",
            "public/js/AdminLTE/dashboard.js"
        )
        .copy(
            "vendor/bower_components/admin-lte/dist/js/demo.js",
            "public/js/AdminLTE/demo.js"
        )
        //copy fonts
        .copy(
            "vendor/bower_components/font-awesome/fonts",
            "public/fonts"
        )
        .copy(
            "vendor/bower_components/bootstrap/fonts",
            "public/fonts"
        )
        //Compile Sass
        .sass("app.scss")
        .sass("frontend.scss" , "public/css/Frontend/frontend.css")
        .sass("adminLTE.scss", "public/css/AdminLTE/AdminLTE.css")
});
